import { Router, Request, Response } from "express";
import { Types } from "mongoose";
import {
  addPhotosForUser,
  findPhotos,
  delPhotos,
} from "../../../services/photoService";
import ApplicationError from "../../../models/ApplicationError";
import { CustomRequest } from "./handlers";

const photoHandlers = Router();

const getPhotoHandler = async (req: CustomRequest, res: Response) => {
  try {
    let page: number = +req.query.page;
    if (page <= 0) {
      page = 1;
    }
    const limit: number = +req.query.maxcount;
    if (limit <= 0) {
      res.status(400).json({
        message: "limit must be positive",
        error: true,
      });
      return;
    }
    const userId = req.userID;
    const userIdForSearch: string = req.query.ownerid?.toString();
    const photos = await findPhotos(userId, userIdForSearch, page, limit);
    res.status(200).json({
      message: "ok",
      error: false,
      photos,
      page,
      maxcount: limit,
    });
  } catch (e) {
    // if (e as typeof ApplicationError) {
      console.log(e);
      res.status(500).json({
        message: "internal_error",
        error: true,
      });
    // } else {
    //   console.log(e);

    // }
  }
};

const addPhotoHandler = async (req: CustomRequest, res: Response) => {
  try {
    const userId = req.userID;
    const photos = await addPhotosForUser(userId);
    res.status(201).json({
      message: "ok",
      error: false,
      photos,
    });
  } catch (e) {
    if (e as typeof ApplicationError) {
      console.log(e);
      res.status(e.httpStatus).json({
        message: e.message,
        error: true,
      });
    } else {
      console.log(e);
      res.status(500).json({
        message: "error",
        error: true,
      });
    }
  }
};

const deletePhotoHandler = async (req: CustomRequest, res: Response) => {
  try {
    const ids: string = req.query.photoid.toString();
    const userId = req.userID;
    const numPhotosDeleted = await delPhotos(userId, ids);
    res.status(200).json({
      message: "ok",
      error: false,
      count: numPhotosDeleted,
    });
  } catch (e) {
    if (e as typeof ApplicationError) {
      console.log(e);
      res.status(e.httpStatus).json({
        message: e.message,
        error: true,
      });
    } else {
      console.log(e);
      res.status(500).json({
        message: "error",
        error: true,
      });
    }
  }
};

photoHandlers.get("/load-photos", addPhotoHandler);
photoHandlers.get("/get-photos", getPhotoHandler);
photoHandlers.get("/delete-photo", deletePhotoHandler);

export default photoHandlers;
