import { Router, Request, Response, NextFunction } from "express";
import { IUserWithPass, IUserWithToken } from "../../../models/User";
import { CustomRequest } from "./handlers";
import ApplicationError from "../../../models/User";
import { loginUser, registerUser } from "../../../services/userService";
import jwt from "jsonwebtoken";
import { Types } from "mongoose";
const userHandlers = Router();

const loginHandler = async (req: Request, res: Response) => {
  const login = req.body.login;
  const password = req.body.password;
  if (!login || !password) {
    res.status(400).json({
      message: "check login and password",
      error: true,
    });
    return;
  }
  // const user: IUserWithPass = req.body;
  try {
    const user = await loginUser(login, password);
    res.status(201).json({
      message: "ok",
      error: false,
      user: {
        id: user._id,
        login: user.login,
        email: user.email,
        registerDate: user.registerDate,
        accessToken: user.token,
      },
    });
  } catch (e) {
    if (e as typeof ApplicationError) {
      res.status(e.httpStatus).json({
        message: e.message,
        error: true,
      });
      return;
    }
    console.log(e);
    res.status(500).json({
      message: "error",
      error: true,
    });
  }
};

const registerHandler = async (req: Request, res: Response) => {
  const user: IUserWithPass = req.body;
  try {
    const newUser = await registerUser(user);
    res.status(201).json({
      message: "ok",
      error: false,
      user: {
        id: newUser._id,
        login: newUser.login,
        email: newUser.email,
        registerDate: newUser.registerDate,
        accessToken: newUser.token,
      },
    });
  } catch (e) {
    if (e as typeof ApplicationError) {
      console.log(e);
      res.status(e.httpStatus).json({
        message: e.message,
        error: true,
      });
    } else {
      console.log(e);
      res.status(500).json({
        message: "error",
        error: true,
      });
    }
  }
};

export const authenticateToken = (
  req: CustomRequest,
  res: Response,
  next: NextFunction
) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) {
    res.status(401).json({
      message: "check token. Token is required.",
      error: true,
    });
    return;
  };
  try {
    jwt.verify(
      token,
      process.env.TOKEN_SECRET as string,
      (err: any, user: any) => {
        if (err) {
          console.log(err);
          res.status(401).json({
            message: "check valid token. Token maybe expired.",
            error: true,
          });
          return;
        }
        // console.log(user)
        if (user?.user_id === "") {
          res.status(401).json({
            message: "check token",
            error: true,
          });
          return;
        }
        req.userID = new Types.ObjectId(user?.user_id);
        next();
      }
    );
  } catch (e) {
    console.log(e);
  }
};

userHandlers.post("/login", loginHandler);
userHandlers.post("/register", registerHandler);

export default userHandlers;
