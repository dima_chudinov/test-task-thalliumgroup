import { Router, Request, Response } from "express";
import { Types } from "mongoose";
import { changeTitleAlbum, delAlbum } from "../../../services/albumService";
import ApplicationError from "../../../models/ApplicationError";
import { CustomRequest } from "./handlers";

const albumHandlers = Router();

const changeTitleAlbumHandler = async (req: CustomRequest, res: Response) => {
  try {
    const userID = req.userID;
    const albumIdStr: string = req.query.new_album_name?.toString();
    if (albumIdStr === "") {
      res.status(400).json({
        message: "albumid is required",
        error: true,
      });
      return
    }
    const newTitle: string = req.query.new_album_name?.toString();
    if (newTitle === "") {
      res.status(400).json({
        message: "new_album_name is required",
        error: true,
      });
      return
    }
    const albumID = new Types.ObjectId(albumIdStr);
    const album = await changeTitleAlbum(userID, albumID, newTitle);
    res.status(200).json({
      message: "ok",
      error: false,
      album,
    });
  } catch (e) {
    if (e as typeof ApplicationError) {
      console.log(e);
      res.status(e.httpStatus).json({
        message: e.message,
        error: true,
      });
    } else {
      console.log(e);
      res.status(500).json({
        message: "error",
        error: true,
      });
    }
  }
};

const deleteAlbumHandler = async (req: Request, res: Response) => {
  try {
    const userId = new Types.ObjectId();
    const albumIdStr: string = req.query.ownerid.toString();
    const albumId = new Types.ObjectId(albumIdStr);
    const photos = await delAlbum(userId, albumId);
    res.status(201).json({
      message: "ok",
      error: false,
      photos,
    });
  } catch (e) {
    if (e as typeof ApplicationError) {
      console.log(e);
      res.status(e.httpStatus).json({
        message: e.message,
        error: true,
      });
    } else {
      console.log(e);
      res.status(500).json({
        message: "error",
        error: true,
      });
    }
  }
};

albumHandlers.get("/change-album-title", changeTitleAlbumHandler);
albumHandlers.get("/delete-album", deleteAlbumHandler);

export default albumHandlers;
