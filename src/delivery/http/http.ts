import express from "express";
import bodyParser from "body-parser"

import userHandlers, {authenticateToken} from "./handlers/userHandlers"
import albumHandlers from "./handlers/albumHandlers"
import photoHandlers from "./handlers/photoHandlers"

const app = express();
const port = process.env.HTTP_PORT;

app.use(bodyParser.json())

app.get("/", (req: any, res: any) => {
  res.send("Server work!");
});

app.use("/", userHandlers, authenticateToken, albumHandlers, photoHandlers)

const runHttpServe = () => {
  app.listen(port, () => {
    console.log(`HTTP Server started at http://localhost:${port}`);
  });
};

export default runHttpServe;
