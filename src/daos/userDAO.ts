import User, { IUserWithPass, IUser } from "../models/User";

export const getUserByLogin = async (
  login: string
): Promise<IUserWithPass[]> => {
  const user = await User.find({
    $or: [{ email: login }, { login }],
  }).exec();
  return user;
};
export const getUserByLoginAndEmail = async (
  login: string,
  email: string
): Promise<IUserWithPass[]> => {
  const user = await User.find({
    $or: [{ email: login }, { login }, { login: email }, { email }],
  }).exec();
  return user;
};

export const createUser = async (
  user: IUserWithPass
): Promise<IUserWithPass> => {
  // let newUser = {} as IUser
  const newUser = await User.create(user);
  return newUser;
};
