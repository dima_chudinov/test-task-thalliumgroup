import Album, { IAlbum } from "../models/Album";
import { Types } from "mongoose"

export const createAlbum = async (album: IAlbum): Promise<IAlbum> => {
  const newAlbum = await Album.create(album);
  return newAlbum;
};

export const updateTitleAlbum = async (album: IAlbum) => {
  const filter = {"owner":album.owner, "_id":album._id}
  const update = {"$set":{"title": album.title}}
  const newAlbum = await Album.updateOne(filter, update).exec();
  return newAlbum;
};

export const deleteAlbums = async (albumIds: Types.ObjectId[], owner: Types.ObjectId) => {
  const filter = {"_id": {"$or":albumIds}, "owner":owner}
  const deleteRes = await Album.deleteMany(filter).exec();
  return deleteRes;
};
