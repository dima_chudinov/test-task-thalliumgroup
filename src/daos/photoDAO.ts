import { Types } from "mongoose";
import Photo, { IPhoto } from "../models/Photo";

export const createPhotos = async (photos: IPhoto[]): Promise<IPhoto[]> => {
  const newPhotos = await Photo.insertMany(photos);
  return newPhotos;
};

export const findPhotos = async (
  ownerID: Types.ObjectId,
  offset: number,
  limit: number
): Promise<IPhoto[]> => {
  const newPhotos = await Photo.find(
    { owner: ownerID },
    {},
    { limit, skip: offset }
  );
  return newPhotos;
};

// export const createUser = async (user: IUserWithPass): Promise<IUserWithPass> => {
//   // let newUser = {} as IUser
//   const newUser = await User.create(user);
//   return newUser;
// };

export const deletePhotos = async (
  photoIds: Types.ObjectId[],
  owner: Types.ObjectId
) => {
  const filter = { _id: { $or: photoIds }, owner };
  const deleteRes = await Photo.deleteMany(filter).exec();
  return deleteRes;
};
