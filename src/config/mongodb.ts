import { connect } from "mongoose";

export const connectToMongoDB = async () => {
  const url = process.env.DB_CONN_STRING;
  const db = process.env.DB_NAME;
  await connect(url + "/" + db);
};
