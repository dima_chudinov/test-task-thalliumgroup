// import {} from "jest"
import {generateHash, isEqualsPasswords} from "../userService"

describe('user auth', () => {
    it('generated hash not equals password which use for generate hash', async ()=>{
        const password = "test123"
        const hash = await generateHash(password)
        expect(password).not.toEqual(hash)
    })
    it('generated password hash must equal user password hash', async ()=>{
        const password = "test123"
        const hash = await generateHash(password)
        const isEqual = await isEqualsPasswords(password, hash)
        expect(isEqual).toBe(true)
    })
})
