import Photo, { IPhoto } from "../models/Photo";
import { IAlbum } from "../models/Album";
import { createAlbum } from "../daos/albumDAO";
import {
  createPhotos,
  deletePhotos,
  findPhotos as findPhotosDAO,
} from "../daos/photoDAO";
import axios from "axios";
import { Types } from "mongoose";

interface PhotoPlaceholder {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

const receivePhotosFromPlaceholder = async (
  albumId: number
): Promise<PhotoPlaceholder[]> => {
  let photos = {} as PhotoPlaceholder[];
  try {
    const { data } = await axios.get(
      "http://jsonplaceholder.typicode.com/photos?albumId=" + albumId
    );
    photos = data;
  } catch (e) {
    console.log(e);
  }
  return new Promise((resolve) => resolve(photos));
};

const albumPrefixName = "Album №";

export const addPhotosForUser = async (
  userID: Types.ObjectId
): Promise<IPhoto[]> => {
  const albumId = 1;
  const photos = await receivePhotosFromPlaceholder(albumId);
  const albumName = albumPrefixName + albumId;
  const newAlbum = await createAlbum({
    _id: new Types.ObjectId(),
    owner: userID,
    title: albumName,
  } as IAlbum);
  const photosForDB = [] as IPhoto[];
  photos.forEach((photo) => {
    photosForDB.push({
      _id: new Types.ObjectId(),
      albumID: newAlbum._id,
      title: photo.title,
      thumbnailUrl: photo.thumbnailUrl,
      url: photo.url,
      owner: userID,
    } as IPhoto);
  });
  const newPhotos = await createPhotos(photosForDB);
  return new Promise((resolve) => {
    resolve(newPhotos);
  });
};

export const findPhotos = async (
  userID: Types.ObjectId,
  userIDForSearchStr: string,
  page: number,
  limit: number
): Promise<IPhoto[]> => {
  // const photos = [] as IPhoto[];
  const offset = page * limit - limit > 0 ? page * limit - limit : 0;
  let userIdForSearch = {} as Types.ObjectId;
  try {
    userIdForSearch = userIDForSearchStr
      ? new Types.ObjectId(userIDForSearchStr)
      : userID;
  } catch (e) {
    userIdForSearch = userID;
  }
  const photos = await findPhotosDAO(userIdForSearch, offset, limit);
  return new Promise((resolve) => {
    resolve(photos);
  });
};
export const delPhotos = async (
  userID: Types.ObjectId,
  id: string
): Promise<number> => {
  const photos = [] as IPhoto[];
  const idsStr = id.split(",");
  const ids = [] as Types.ObjectId[];
  idsStr.forEach((idStr) => {
    try {
      if (idStr !== "") {
        ids.push(new Types.ObjectId(idStr));
      }
    } catch (e) {
      console.log(e)
    }
  });
  const deletedPhotos = await deletePhotos(ids, userID);
  return new Promise((resolve) => {
    resolve(deletedPhotos.deletedCount);
  });
};
