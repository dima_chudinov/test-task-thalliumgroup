export const errPasswordsNotEquals = new Error("passwords_not_equals");
export const errInternalError = new Error("internal_error");
export const errUserNotFound = new Error("user_not_found");
export const errUserAlreadyExists = new Error("user_already_exists");
export const errNoPermissions = new Error("no_permissions")