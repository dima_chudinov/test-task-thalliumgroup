import { IAlbum } from "../models/Album";
import { createAlbum, updateTitleAlbum } from "../daos/albumDAO";
import { Types } from "mongoose";
import ApplicationError from "../models/ApplicationError";
import { IApplicationError } from "../models/ApplicationError";
import {errNoPermissions, errInternalError} from "./errors"

export const changeTitleAlbum = async (
  userId: Types.ObjectId,
  albumId: Types.ObjectId,
  newTitle: string
): Promise<IAlbum> => {
  try {
    const album = await updateTitleAlbum(<IAlbum>{
      owner: userId,
      _id: albumId,
      title: newTitle,
    });
    if (album.modifiedCount === 0) {
      throw new ApplicationError(<IApplicationError>{
        httpStatus: 403,
        message:errNoPermissions.message,
        error: errNoPermissions
      });
    }
  } catch (e) {
    if (e as typeof ApplicationError) {
      throw e;
    }
    throw new ApplicationError(<IApplicationError>{
      httpStatus: 500,
      message: errInternalError.message,
      error: errInternalError
    })
  }
  return new Promise((resolve) => {
    resolve({} as IAlbum);
  });
};

export const delAlbum = async (
  userId: Types.ObjectId,
  albumId: Types.ObjectId
): Promise<IAlbum> => {
  return new Promise((resolve) => {
    resolve({} as IAlbum);
  });
};
