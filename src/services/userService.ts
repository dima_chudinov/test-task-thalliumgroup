import ApplicationError, {
  IApplicationError,
} from "../models/ApplicationError";
import { getUserByLogin, createUser, getUserByLoginAndEmail } from "../daos/userDAO";
import { compare, hash } from "bcrypt";
import { IUser, IUserWithPass, IUserWithToken } from "../models/User";
import { Types } from "mongoose";
import jwt from "jsonwebtoken"
import {errInternalError, errPasswordsNotEquals, errUserAlreadyExists, errUserNotFound} from "./errors"

export const loginUser = async (
  login: string,
  password: string
): Promise<IUserWithToken> => {
  const appErr = {
    httpStatus: 200,
    message: "ok",
  } as IApplicationError;
  let resUser = {} as IUserWithToken;
  try {
    const users = await getUserByLogin(login);
    if (users.length < 1) {
      throw new ApplicationError({
        httpStatus: 404,
        message: errUserNotFound.message,
        error: errUserNotFound,
      } as IApplicationError);
    }
    const user = users[0];
    const isPasswordsEquals = await isEqualsPasswords(password, user.password);
    if (!isPasswordsEquals) {
      throw new ApplicationError({
        httpStatus: 401,
        message: errPasswordsNotEquals.message,
        error: errPasswordsNotEquals,
      } as IApplicationError);
    }
    user.password = undefined;
    resUser = user as IUserWithToken;
    resUser.token = generateAccessToken(resUser._id)
  } catch (e) {
    if (e as typeof ApplicationError) {
      throw e;
    }
    throw new ApplicationError({
      httpStatus: 500,
      message: errInternalError.message,
      error: e,
    } as IApplicationError);
  }
  return new Promise((resolve) => {
    resolve(resUser);
  });
};

export const registerUser = async (
  userForCreating: IUserWithPass
): Promise<IUserWithToken> => {
  const appErr = {
    httpStatus: 201,
    message: "ok",
  } as IApplicationError;
  let resUser = {} as IUserWithToken;
  userForCreating._id = new Types.ObjectId();
  userForCreating.password = await generateHash(userForCreating.password);
  try {
    const users = await getUserByLoginAndEmail(userForCreating.login, userForCreating.email);
    if (users.length > 0) {
      throw new ApplicationError({
        httpStatus: 400,
        message: errUserAlreadyExists.message,
        error: errUserAlreadyExists,
      } as IApplicationError);
    }
    const user = await createUser(userForCreating);
    resUser = user as IUserWithToken;
    resUser.password = undefined;
    resUser.token = generateAccessToken(resUser._id)
  } catch (e) {
    if (e as typeof ApplicationError) {
      throw e;
    }
    throw new ApplicationError({
      httpStatus: 500,
      message: errInternalError.message,
      error: e,
    } as IApplicationError);
  }
  return new Promise((resolve) => {
    resolve(resUser);
  });
};

export const isEqualsPasswords = async (
  passwordFromUser: string,
  passwordHashFromDB: string
): Promise<boolean> => {
  return compare(passwordFromUser, passwordHashFromDB);
};

export const generateHash = async (password: string): Promise<string> => {
  const salt = process.env.PASSWORD_SALT;
  const passwordHash = await hash(password, 10);
  return passwordHash;
};

const generateAccessToken = (id:Types.ObjectId) => {
  return jwt.sign({"user_id":id}, process.env.TOKEN_SECRET, { expiresIn: '1800s' });
}