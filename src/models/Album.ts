import { Schema, model, Types } from "mongoose";

export interface IAlbum {
  _id: Types.ObjectId;
  owner: Types.ObjectId;
  title: string;
}

const schema = new Schema<IAlbum>(
  {
    _id: { type: Types.ObjectId, required: true },
    owner: { type: Types.ObjectId, required: true },
    title: { type: String, required: true },
  },
  { autoIndex: false }
);

// Create unique index. Unique album name for user
schema.index({owner:1, title:1},{unique: true});
// Index for update tietle album
schema.index({_id:1, owner:1});

const AlbumModel = model<IAlbum>("albums", schema);


export default AlbumModel;
