export interface IApplicationError extends Error{
    error?: Error;
    message: string;
    httpStatus: number;
}

export default class ApplicationError extends Error{
    public error: Error;
    public message: string;
    public httpStatus: number;
    constructor (appErr: IApplicationError) {
        super()
        this.error = appErr.error;
        this.message = appErr.message;
        this.httpStatus  = appErr.httpStatus;
    }
}