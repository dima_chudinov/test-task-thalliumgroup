import { Schema, model, Types } from "mongoose";
export interface IUser {
  _id: Types.ObjectId;
  login: string;
  email: string;
  registerDate: Date;
}
export interface IUserWithPass extends IUser {
  password: string;
}
export interface IUserWithToken extends IUserWithPass {
  token: string;
}
const schema = new Schema<IUserWithPass>(
  {
    _id: { type: Types.ObjectId, required: true },
    login: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    registerDate: { type: Date, required: true, default: new Date(Date.now()) },
  },
  { autoIndex: false }
);

// Create unique index
// Unique for login
schema.index({login:1},{unique: true});
// Unique for email
schema.index({email:1},{unique: true});

// // Index for search by login OR email
// schema.index({ login: 1 });
// schema.index({ email: 1 });

const User = model<IUserWithPass>("users", schema);

export default User;
