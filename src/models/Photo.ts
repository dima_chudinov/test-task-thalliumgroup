import { Schema, model, Types } from "mongoose";

export interface IPhoto {
  _id: Types.ObjectId;
  albumID: Types.ObjectId;
  owner: Types.ObjectId;
  title: string;
  url: string;
  thumbnailUrl: string;
}

const schema = new Schema<IPhoto>(
  {
    _id: { type: Types.ObjectId, required: true },
    albumID: { type: Types.ObjectId, required: true },
    owner: { type: Types.ObjectId, required: true },
    title: { type: String, required: true },
    url: { type: String, required: true },
    thumbnailUrl: { type: String, required: true },
  },
  { autoIndex: false }
);

const PhotoModel = model<IPhoto>("photos", schema);

export default PhotoModel;
