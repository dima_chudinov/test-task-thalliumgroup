import HTTPServer from "./delivery/http/http";
import * as dotenv from "dotenv";
import { connectToMongoDB } from "./config/mongodb";

dotenv.config();
console.log("Starting...");

connectToMongoDB().then();

HTTPServer();
