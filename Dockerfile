FROM node:16-alpine

RUN apk update
WORKDIR /app

COPY package*.json ./
COPY tsconfig.json ./
RUN npm install

COPY src /app/src

RUN ls -a

EXPOSE 8085

CMD [ "npm", "run", "start" ]